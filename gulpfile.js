'use strict'

const { series, src, dest, watch } = require('gulp')
const fs = require('fs')
const connect = require('gulp-connect')
const generator = require('@antora/site-generator-default')
const { reload: livereload } = process.env.LIVERELOAD === 'true' ? require('gulp-connect') : {}
const inject = require('gulp-inject')
const path = require('path')
const rename = require('gulp-rename')
const sort = require('gulp-sort')
const yaml = require('js-yaml')
const exec = require('child_process').exec

const playbookFilename = process.env.SITE_ENV ? `${process.env.SITE_ENV}playbook.yaml` : 'local-playbook.yaml'
const playbook = yaml.safeLoad(fs.readFileSync(playbookFilename, 'utf8'))
const outputDir = (playbook.output || {}).dir || './build/site'
// const valeDirs = playbook.content.sources[0].start_paths.toString().replace(/,/g, ' ')
const serverConfig = { name: 'Preview Site', livereload, port: 5000, root: outputDir }
const antoraArgs = process.env.CONTEXT === 'deploy-preview' ? ['--playbook', playbookFilename, '--url', process.env.DEPLOY_PRIME_URL] : ['--playbook', playbookFilename]
const watchPatterns = playbook.content.sources.filter((source) => !source.url.includes(':')).reduce((accum, source) => {
  accum.push(`${source.url}/${source.start_path ? source.start_path + '/' : ''}playbook.yml`)
  accum.push(`${source.url}/${source.start_path ? source.start_path + '/' : ''}**/*.adoc`)
  return accum
}, [])


function titleFrom(file) {
  const maybeName = /(?:=|#) (.*)/.exec(file.contents.toString())
  if (maybeName == null) {
      throw new Error(`${file.path} doesn't contain Asciidoc heading ('= <Title>') or ('# <Title')`);
  }

  return maybeName[1];
}

function generate (done) {
  generator(antoraArgs, process.env)
    .then(() => done(), console.log('Generated from', playbookFilename))
    .catch((err) => {
      console.log(err)
      done()
    })
}

function serve (done) {
  connect.server(serverConfig, function () {
    this.server.on('close', done)
    watch(watchPatterns, generate)
    if (livereload) watch(this.root).on('change', (filepath) => src(filepath, { read: false }).pipe(livereload()))
  })
}


exports.default = series(generate, serve)
exports.ci = series(generate)
exports.generate = generate